
matrice = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]


for ligne in range(len(matrice)):
    for colonne in range(len(matrice[ligne])):
        element = matrice[ligne][colonne]
        print(f"Élément LIGNE:{ligne + 1}  --- COLONNE:{colonne + 1}==== {element}")


# Avec while
matrice = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

ligne = 0
colonne = 0

while ligne < len(matrice):
    colonne = 0 
    while colonne < len(matrice[ligne]):
        element = matrice[ligne][colonne]
        print(f"Élément LIGNE:{ligne + 1}  --- COLONNE:{colonne + 1}==== {element}")
        colonne += 1
    ligne += 1
